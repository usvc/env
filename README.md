# `env`

A base environment setup for cloud-native development.

## Services Index

| Service Name | Type | URL |
| --- | --- | --- |
| Gitea | Repository | [http://localhost:33000](http://localhost:33000) |
| Drone CI | CI Pipeline | [http://localhost:30080](http://localhost:30080) |
| Prometheus | Metrics Collector | [http://localhost:29090](http://localhost:29090) |
| Grafana | Metrics Visualiser | [http://localhost:23000](http://localhost:23000) |
| Kibana | Logs Viewer | [http://localhost:35601](http://localhost:35601) |

