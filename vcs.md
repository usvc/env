# Version Control System

## Initialising

Run `make base_start` to initialise the base environment.

Run `make vcs_start` to start the Version Control System environment.

## Setting Up

- Access Gitea at [http://localhost:33000](http://localhost:33000)
- Click on **Sign in** on the right of the top navigation bar (or navigate to [http://localhost:33000/install](http://localhost:33000/install))
- Scroll to the bottom and click **Install Gitea** (this might take about 5 seconds or more)
- Click on **Register** near the right of the top navigation bar (or navigate to [http://localhost:33000/user/sign_up](http://localhost:33000/user/sign_up))
- For the **Username** enter `username`
- For the **Email Address** enter `email@domain.com`
- For the **Password** enter `P@55w0rd` 
- Re-type the password in **Re-Type Password**
- Click on **Register Account** and you should see *Account was successfully created.*

## Troubleshooting

The following are some issues you might face

### The database settings are invalid: Error 1049: Unknown database 'gitea'

To remedy this, run `make vcs_clean`, ensure that `~/data/gitea/mysql/data` stays empty of all files other than `.gitignore` for about 5 seconds (waiting for Gitea's MySQL to spin down), then run `make vcs` up again.

### Webook failed

If you've connected and disconnected the `cicd` component before, this might cause the nginx reverse proxy to be unable to resume the connection. To fix this, run `make vcs_reload` which should run the `nginx -s reload` command inside the gitea container.
