module gitlab.com/usvc/env

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	github.com/sanity-io/litter v1.2.0 // indirect
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.2.2
)
