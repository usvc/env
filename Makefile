STACK_ID=usvc_base
GITEA_CLIENT_ID=
GITEA_CLIENT_SECRET=

# define the GITEA_CLIENT_ID and GITEA_CLIENT_SECRET in
# the Makefile.properties to avoid committing secrets to
# the main repository
-include Makefile.properties

init:
	-@docker swarm init
	@$(MAKE) base
denit:
	-@docker stack rm $(STACK_ID)
	@docker swarm leave
reset:
	-@$(MAKE) denit
	@$(MAKE) init
status:
	@docker stack ps $(STACK_ID) \
		--no-trunc \
		--filter "desired-state=Running" \
		--format "[{{ .Name }}]\n  image: {{ .Image }}\n  ports: {{ .Ports }}\n  state: {{ .CurrentState }}"
top:
	@docker stats $(docker ps | grep usvc_base | cut -f 1 -d ' ')

base:
	@$(MAKE) .deploy FILEPATH=./base.yml
base_start:
	@$(MAKE) .start FILEPATH=./base.yml
base_stop:
	@$(MAKE) .stop FILEPATH=./base.yml
base_purge:
	-@docker stop $$(docker ps | grep $(STACK_ID)_drone | cut -f 1 -d' ')
	-@docker service ls | grep drone | cut -f 1 -d' ' | xargs docker service rm
cicd:
	@$(MAKE) .deploy FILEPATH=./cicd.yml
cicd_start:
	@$(MAKE) .build FILEPATH=./cicd.yml
	@DRONE_GITEA_CLIENT_ID=$(GITEA_CLIENT_ID) \
		DRONE_GITEA_CLIENT_SECRET=$(GITEA_CLIENT_SECRET) \
		$(MAKE) .start FILEPATH=./cicd.yml
cicd_stop:
	@$(MAKE) .stop FILEPATH=./cicd.yml
cicd_clean:
	sudo rm -rf ./data/drone/data/*
cicd_purge:
	-@docker stop $$(docker ps | grep $(STACK_ID)_drone | cut -f 1 -d' ')
	-@docker service ls | grep drone | cut -f 1 -d' ' | xargs docker service rm
	@$(MAKE) cicd_clean
gateway:
	@$(MAKE) .deploy FILEPATH=./gateway.yml
mysql:
	@$(MAKE) .deploy FILEPATH=./mysql.yml
logging:
	@$(MAKE) .deploy FILEPATH=./logging.yml
metrics:
	@$(MAKE) .deploy FILEPATH=./metrics.yml
vcs:
	@$(MAKE) .build FILEPATH=./vcs.yml
	@$(MAKE) .deploy FILEPATH=./vcs.yml
vcs_start:
	@$(MAKE) .build FILEPATH=./vcs.yml
	@$(MAKE) .start FILEPATH=./vcs.yml
vcs_stop:
	@$(MAKE) .stop FILEPATH=./vcs.yml
vcs_reload:
	docker exec -it $$(docker ps | grep local/gitea | cut -f 1 -d' ') nginx -s reload
vcs_clean:
	sudo rm -rf \
		./data/gitea/mysql/data/* \
		./data/gitea/data/git/lfs \
		./data/gitea/data/git/repositories \
		./data/gitea/data/gitea/indexers \
		./data/gitea/data/gitea/conf/* \
		./data/gitea/data/gitea/log/*
vcs_purge:
	-@docker service ls | grep gitea | cut -f 1 -d' ' | xargs docker service rm
	-@docker stop $$(docker ps | grep $(STACK_ID)_gitea | cut -f 1 -d' ')
	@$(MAKE) vcs_clean

.deploy:
	@USER_ID=$$(id -u) docker stack deploy --compose-file ${FILEPATH} $(STACK_ID)
.start:
	@USER_ID=$$(id -u) docker-compose -f ${FILEPATH} up
.stop:
	@USER_ID=$$(id -u) docker-compose -f ${FILEPATH} down
.build:
	@USER_ID=$$(id -u) docker-compose -f ${FILEPATH} build
