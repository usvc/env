#!/bin/sh

CONFIG_PATH="${GITEA_CUSTOM}/conf/app.ini";

export APP_NAME="${APP_NAME:-Gitea}"
export RUN_MODE="${RUN_MODE:-prod}"
export RUN_USER="${RUN_USER:-gitea}"

echo '[server]' > ${CONFIG_PATH};
export SERVER_APP_DATA_PATH="${SERVER_APP_DATA_PATH:-${GITEA_CUSTOM}}";
echo "APP_DATA_PATH=${SERVER_APP_DATA_PATH}" > ${CONFIG_PATH};

export SERVER_SSH_DOMAIN