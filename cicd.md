# CI/CD

## Initialisation

Before beginning you should have already run `make init` and `make vcs`.

Also confirm that you have at least one registered user on Gitea by following the [Setting Up section of `vcs.md`](./vcs.md#setting-up) before proceeding with registration of an OAuth2 application:

1. Sign into Gitea at [http://localhost:33000/user/login?redirect_to=](http://localhost:33000/user/login?redirect_to=)
2. On the right side of the top navigation bar, click on the user icon and click on **Settings**
3. Navigate to the **Applications** tab on the secondary top navigation bar
4. Under the section **Manage OAuth2 Applications** with sub-section **Create a new OAuth2 Application**, enter `drone` as the **Application Name**, and enter `http://localhost:30080/login` as the **Redirect URI** field
5. Click on **Create Application**
6. You should see a **Client ID** and **Client Secret**
7. Copy the **Client ID**, create a file named `Makefile.properties` and paste it as the value in `GITEA_CLIENT_ID` environment variable at the top of the file so that it looks like `GITEA_CLIENT_ID=<pasted value>`
8. Copy the **Client Secret**, add the value in the `Makefile.properties` file created in the above step as the value to the `GITEA_CLIENT_SECRET` environment variable at the top of the file so that it looks like `GITEA_CLIENT_SECRET=<pasted value>`
9. To start the CI/CD services, run `make cicd_start`

## Setting Up

After completing the above steps, the Drone server should be available at [http://localhost:30080](http://localhost:30080). Navigating there, you should be redirected to your [Gitea instance](http://localhost:33000) for an authorization page.

> If you see *Authorization failed - Client ID not registered*, you've probably started the `make cicd_start` before saving the Makefile with the `GITEA_CLIENT_ID` and `GITEA_CLIENT_SECRET` set. Do a keyboard interrupt on the terminal and re-run `make cicd_start`

If all goes well, click on **Authorize Application** and you will be redirected back to the Drone CI's web UI.

Then follow the next steps:

1. Go back to [Gitea at localhost:33000](http://localhost:33000) and create a new repository by clicking the **+** sign on the top navigation bar, followed by **New Repository**.
2. Enter `somerepo` for the **Repository Name** field and click **Create Repository**
3. Your repository should now be available at [http://localhost:33000/username/somerepo](http://localhost:33000/username/somerepo) assuming your username has been set to `username`
4. Push to our new remote by running `git push gitea master`
5. Go to [Drone CI at localhost:30080](http://localhost:30080), click on **Sync** at the top right of the main page and wait for your repository to appear
6. Once your repository is visible, click on the **Activate** button, you should be brought to the settings page in the Drone UI
7. Click on **Activate Repository**, which will trigger a call to Gitea to create a webhook on the repository you created
8. In this repository, add a new remote by running `git remote add gitea http://username@localhost:33000/username/somerepo.git`
9. Test the integration by pushing this repository - which contains an application written in Go - to the `gitea` remote: `git push gitea master`


## References

[Official documentation for Drone on Gitea](https://docs.drone.io/server/provider/gitea/)