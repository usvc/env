package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/usvc/env/src/lib/api"
	"gitlab.com/usvc/env/src/lib/math"
)

func handleAdd(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	this := params["this"]
	toThis := params["toThis"]
	result, err := math.Add(this, toThis)
	if api.HandleError(err, w) {
		return
	}
	api.HandleResponse(result, w)
}

func handleMinus(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	fromThis := params["fromThis"]
	this := params["this"]
	result, err := math.Minus(fromThis, this)
	if api.HandleError(err, w) {
		return
	}
	api.HandleResponse(result, w)
}

func handleMultiply(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	this := params["this"]
	byThis := params["byThis"]
	result, err := math.Multiply(this, byThis)
	if api.HandleError(err, w) {
		return
	}
	api.HandleResponse(result, w)
}

func handleDivide(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	this := params["this"]
	byThis := params["byThis"]
	result, err := math.Divide(this, byThis)
	if api.HandleError(err, w) {
		return
	}
	api.HandleResponse(result, w)
}
