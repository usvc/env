package main

import (
	"log"
)

var (
	commitHash     string
	version        string
	buildTimestamp string
)

const (
	// ConfigKeyServerAddr defines the configuration to .GetString(...) on
	ConfigKeyServerAddr = "server_addr"
	// ConfigKeyPrecision defines the configuration to .GetInt(...) on
	ConfigKeyPrecision = "precision"
)

func main() {
	config := initialiseConfigurations()
	handler := getRouter(config)
	server := getServer(config, handler)

	log.Printf("attempting to bind to address %s\n", config.GetString(ConfigKeyServerAddr))
	if err := server.ListenAndServe(); err != nil {
		panic(err)
	}
}
