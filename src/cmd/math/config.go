package main

import "github.com/spf13/viper"

func initialiseConfigurations() *viper.Viper {
	config := viper.New()
	config.SetDefault(ConfigKeyServerAddr, "0.0.0.0:34567")
	config.SetDefault(ConfigKeyPrecision, 3)
	config.AutomaticEnv()
	return config
}
