package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/suite"
)

type RouterTests struct {
	suite.Suite
}

func TestRouter(t *testing.T) {
	suite.Run(t, &RouterTests{})
}

func (s *RouterTests) Test_getRouter() {
	router := getRouter(viper.New())
	server := httptest.NewServer(router)
	defer server.Close()
	serverURL, err := url.Parse(server.URL)
	s.Nil(err)
	serverURL.Path = "/add/1/2"
	response, err := http.Get(serverURL.String())
	s.Nil(err)
	responseBody, err := ioutil.ReadAll(response.Body)
	s.Nil(err)
	var responseStruct map[string]interface{}
	err = json.Unmarshal(responseBody, &responseStruct)
	s.Nil(err)
	s.Equal(float64(3), responseStruct["result"])

	serverURL.Path = "/minus/1/2"
	response, err = http.Get(serverURL.String())
	s.Nil(err)
	responseBody, err = ioutil.ReadAll(response.Body)
	s.Nil(err)
	err = json.Unmarshal(responseBody, &responseStruct)
	s.Nil(err)
	s.Equal(float64(-1), responseStruct["result"])

	serverURL.Path = "/multiply/1/2"
	response, err = http.Get(serverURL.String())
	s.Nil(err)
	responseBody, err = ioutil.ReadAll(response.Body)
	s.Nil(err)
	err = json.Unmarshal(responseBody, &responseStruct)
	s.Nil(err)
	s.Equal(float64(2), responseStruct["result"])

	serverURL.Path = "/divide/1/2"
	response, err = http.Get(serverURL.String())
	s.Nil(err)
	responseBody, err = ioutil.ReadAll(response.Body)
	s.Nil(err)
	err = json.Unmarshal(responseBody, &responseStruct)
	s.Nil(err)
	s.Equal(float64(0.5), responseStruct["result"])
}
