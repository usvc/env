package main

import (
	"os"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/suite"
)

type ConfigTests struct {
	suite.Suite
}

func TestConfig(t *testing.T) {
	suite.Run(t, &ConfigTests{})
}

func (s *ConfigTests) Test_initialiseConfigurations_defaults() {
	config := initialiseConfigurations()
	s.Equal("0.0.0.0:34567", config.GetString(ConfigKeyServerAddr))
	s.Equal(3, config.GetInt(ConfigKeyPrecision))
}

func (s *ConfigTests) Test_initialiseConfigurations() {
	expectedServerAddr := "255.255.255.255:12345"
	expectedPrecision := "42"
	expectedPrecisionInt, err := strconv.Atoi(expectedPrecision)
	s.Nil(err)
	os.Setenv(strings.ToUpper(ConfigKeyServerAddr), expectedServerAddr)
	os.Setenv(strings.ToUpper(ConfigKeyPrecision), expectedPrecision)
	defer func() {
		os.Setenv(strings.ToUpper(ConfigKeyServerAddr), "")
		os.Setenv(strings.ToUpper(ConfigKeyPrecision), "")
	}()
	config := initialiseConfigurations()
	s.Equal(expectedServerAddr, config.GetString(ConfigKeyServerAddr))
	s.Equal(expectedPrecisionInt, config.GetInt(ConfigKeyPrecision))
}
