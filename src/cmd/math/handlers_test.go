package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/suite"
)

type HandlersTests struct {
	suite.Suite
}

func TestHandlers(t *testing.T) {
	suite.Run(t, &HandlersTests{})
}

func (s *HandlersTests) Test_handleAdd() {
	responseWriter, testRequest :=
		s.createHTTPArtifacts(map[string]string{
			"this":   "1",
			"toThis": "2",
		})
	handleAdd(responseWriter, testRequest)
	response := responseWriter.Result()
	s.Equal(http.StatusOK, response.StatusCode)
	s.assertResultEquals(3, responseWriter.Body)
}

func (s *HandlersTests) Test_handleAdd_errors() {
	responseWriter, testRequest :=
		s.createHTTPArtifacts(map[string]string{
			"this":   "a",
			"toThis": "2",
		})
	handleAdd(responseWriter, testRequest)
	response := responseWriter.Result()
	s.Equal(http.StatusInternalServerError, response.StatusCode)
	s.assertParsingSyntaxError(responseWriter.Body)
}

func (s *HandlersTests) Test_handleMinus() {
	responseWriter, testRequest :=
		s.createHTTPArtifacts(map[string]string{
			"fromThis": "2",
			"this":     "1",
		})
	handleMinus(responseWriter, testRequest)
	response := responseWriter.Result()
	s.Equal(http.StatusOK, response.StatusCode)
	s.assertResultEquals(1, responseWriter.Body)
}

func (s *HandlersTests) Test_handleMinus_errors() {
	responseWriter, testRequest :=
		s.createHTTPArtifacts(map[string]string{
			"fromThis": "2",
			"this":     "a",
		})
	handleMinus(responseWriter, testRequest)
	response := responseWriter.Result()
	s.Equal(http.StatusInternalServerError, response.StatusCode)
	s.assertParsingSyntaxError(responseWriter.Body)
}

func (s *HandlersTests) Test_handleMultiply() {
	responseWriter, testRequest :=
		s.createHTTPArtifacts(map[string]string{
			"this":   "3.52",
			"byThis": "2",
		})
	handleMultiply(responseWriter, testRequest)
	response := responseWriter.Result()
	s.Equal(http.StatusOK, response.StatusCode)
	s.assertResultEquals(7.04, responseWriter.Body)
}

func (s *HandlersTests) Test_handleMultiply_errors() {
	responseWriter, testRequest :=
		s.createHTTPArtifacts(map[string]string{
			"this":   "a",
			"byThis": "2",
		})
	handleMultiply(responseWriter, testRequest)
	response := responseWriter.Result()
	s.Equal(http.StatusInternalServerError, response.StatusCode)
	s.assertParsingSyntaxError(responseWriter.Body)
}

func (s *HandlersTests) Test_handleDivide() {
	responseWriter, testRequest :=
		s.createHTTPArtifacts(map[string]string{
			"this":   "1.001",
			"byThis": "2",
		})
	handleDivide(responseWriter, testRequest)
	response := responseWriter.Result()
	s.Equal(http.StatusOK, response.StatusCode)
	s.assertResultEquals(0.5005, responseWriter.Body)
}

func (s *HandlersTests) Test_handleDivide_errors() {
	responseWriter, testRequest :=
		s.createHTTPArtifacts(map[string]string{
			"this":   "a",
			"byThis": "2",
		})
	handleDivide(responseWriter, testRequest)
	response := responseWriter.Result()
	s.Equal(http.StatusInternalServerError, response.StatusCode)
	s.assertParsingSyntaxError(responseWriter.Body)
}

func (s *HandlersTests) createHTTPArtifacts(params map[string]string) (*httptest.ResponseRecorder, *http.Request) {
	responseWriter := httptest.NewRecorder()
	testRequest, err := http.NewRequest("GET", "/", nil)
	s.Nil(err)
	testRequest = mux.SetURLVars(testRequest, params)
	return responseWriter, testRequest
}

func (s *HandlersTests) assertResultEquals(this float64, result *bytes.Buffer) {
	var responseStruct map[string]interface{}
	err := json.Unmarshal(result.Bytes(), &responseStruct)
	s.Nil(err)
	s.Nil(responseStruct["error"])
	s.Len(responseStruct["message"], 0)
	s.Equal(this, responseStruct["result"])
}

func (s *HandlersTests) assertParsingSyntaxError(result *bytes.Buffer) {
	var responseStruct map[string]interface{}
	err := json.Unmarshal(result.Bytes(), &responseStruct)
	s.Nil(err)
	s.NotNil(responseStruct["error"])
	s.Contains(responseStruct["error"], "invalid syntax")
}
