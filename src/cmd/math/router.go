package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

func getRouter(config *viper.Viper) http.Handler {
	handler := mux.NewRouter()
	handler.HandleFunc("/add/{this}/{toThis}", handleAdd)
	handler.HandleFunc("/minus/{fromThis}/{this}", handleMinus)
	handler.HandleFunc("/multiply/{this}/{byThis}", handleMultiply)
	handler.HandleFunc("/divide/{this}/{byThis}", handleDivide)
	return handler
}
