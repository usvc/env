package main

import (
	"testing"

	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/suite"
)

type ServerTests struct {
	suite.Suite
}

func TestServer(t *testing.T) {
	suite.Run(t, &ServerTests{})
}

func (s *ServerTests) Test_getServer() {
	config := viper.New()
	config.SetDefault(ConfigKeyServerAddr, "_serveraddr")
	handler := mux.NewRouter()
	server := getServer(config, handler)
	s.Equal("_serveraddr", server.Addr)
	s.NotNil(server.Handler)
}
