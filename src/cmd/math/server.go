package main

import (
	"net/http"

	"github.com/spf13/viper"
)

func getServer(config *viper.Viper, handler http.Handler) *http.Server {
	server := &http.Server{
		Addr:    config.GetString(ConfigKeyServerAddr),
		Handler: handler,
	}
	return server
}
