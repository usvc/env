package math

import (
	"strconv"
)

// ConstantFloatSize defines the size of the float we use in calculations
const ConstantFloatSize = 64

// Add adds :this to :toThis
func Add(this, toThis string) (float64, error) {
	thisNumber, err := strconv.ParseFloat(this, ConstantFloatSize)
	if err != nil {
		return float64(0), err
	}
	toThisNumber, err := strconv.ParseFloat(toThis, ConstantFloatSize)
	if err != nil {
		return float64(0), err
	}
	return thisNumber + toThisNumber, nil
}

// Minus subracts :this from :fromThis
func Minus(fromThis, this string) (float64, error) {
	thisNumber, err := strconv.ParseFloat(this, ConstantFloatSize)
	if err != nil {
		return float64(0), err
	}
	fromThisNumber, err := strconv.ParseFloat(fromThis, ConstantFloatSize)
	if err != nil {
		return float64(0), err
	}
	return fromThisNumber - thisNumber, nil
}

// Multiply multiplies :this by :byThis
func Multiply(this, byThis string) (float64, error) {
	thisNumber, err := strconv.ParseFloat(this, ConstantFloatSize)
	if err != nil {
		return float64(0), err
	}
	byThisNumber, err := strconv.ParseFloat(byThis, ConstantFloatSize)
	if err != nil {
		return float64(0), err
	}
	return thisNumber * byThisNumber, nil
}

// Divide divides :this by :byThis
func Divide(this, byThis string) (float64, error) {
	thisNumber, err := strconv.ParseFloat(this, ConstantFloatSize)
	if err != nil {
		return float64(0), err
	}
	byThisNumber, err := strconv.ParseFloat(byThis, ConstantFloatSize)
	if err != nil {
		return float64(0), err
	}
	return thisNumber / byThisNumber, nil
}
