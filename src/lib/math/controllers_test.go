package math

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type ControllersTests struct {
	suite.Suite
}

func TestControllers(t *testing.T) {
	suite.Run(t, &ControllersTests{})
}

func (s *ControllersTests) Test_Add() {
	res, _ := Add("0", "1")
	s.Equal(float64(1), res)
	res, err := Add("0", "0")
	s.Equal(float64(0), res)
	s.Nil(err)
	res, _ = Add("1", "2")
	s.Equal(float64(3), res)
	res, _ = Add("2", "1")
	s.Equal(float64(3), res)
	res, _ = Add("1.0000000000001", "2")
	s.Equal(float64(3.0000000000001), res)
	res, _ = Add("1.00000000000001", "2")
	s.Equal(float64(3+98e-16), res)
}

func (s *ControllersTests) Test_Add_errors() {
	res, err := Add("a", "1")
	s.NotNil(err)
	s.Equal(float64(0), res)
	res, err = Add("1", "a")
	s.NotNil(err)
	s.Equal(float64(0), res)
}

func (s *ControllersTests) Test_Minus() {
	res, _ := Minus("1", "0")
	s.Equal(float64(1), res)
	res, err := Minus("0", "0")
	s.Equal(float64(0), res)
	s.Nil(err)
	res, _ = Minus("2", "1")
	s.Equal(float64(1), res)
	res, _ = Minus("1", "2")
	s.Equal(float64(-1), res)
	res, _ = Minus("1", "0.0000000000001")
	s.Equal(float64(0.9999999999999), res)
	res, _ = Minus("1", "0.0000000000000000001")
	s.Equal(float64(0.999999999999999999999999), res)
	res, _ = Minus("0", "0.0000000000000001")
	s.Equal(float64(-1e-16), res)
}

func (s *ControllersTests) Test_Minus_errors() {
	res, err := Minus("a", "1")
	s.NotNil(err)
	s.Equal(float64(0), res)
	res, err = Minus("1", "a")
	s.NotNil(err)
	s.Equal(float64(0), res)
}

func (s *ControllersTests) Test_Multiply() {
	res, _ := Multiply("2", "1")
	s.Equal(float64(2), res)
	res, err := Multiply("1", "0")
	s.Equal(float64(0), res)
	s.Nil(err)
	res, _ = Multiply("0.99", "2")
	s.Equal(float64(1.98), res)
	res, _ = Multiply("0.00000000000000005", "2")
	s.Equal(float64(1e-16), res)
}

func (s *ControllersTests) Test_Multiply_errors() {
	res, err := Multiply("a", "1")
	s.NotNil(err)
	s.Equal(float64(0), res)
	res, err = Multiply("1", "a")
	s.NotNil(err)
	s.Equal(float64(0), res)
}

func (s *ControllersTests) Test_Divide() {
	res, _ := Divide("2", "1")
	s.Equal(float64(2), res)
	res, err := Divide("0", "1")
	s.Equal(float64(0), res)
	s.Nil(err)
	res, err = Divide("1", "10000000000000000")
	s.Equal(float64(1e-16), res)
}

func (s *ControllersTests) Test_Divide_errors() {
	res, err := Divide("a", "1")
	s.NotNil(err)
	s.Equal(float64(0), res)
	res, err = Divide("1", "a")
	s.NotNil(err)
	s.Equal(float64(0), res)
}
