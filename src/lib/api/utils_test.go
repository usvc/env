package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"
)

type UtilsTests struct {
	suite.Suite
}

func TestUtils(t *testing.T) {
	suite.Run(t, &UtilsTests{})
}

func (s *UtilsTests) Test_CreateResponse() {
	response := CreateResponse(int(1), nil, "")
	s.Equal(int(1), response["result"])
	s.Nil(response["error"])
	s.Len(response["message"], 0)

	response = CreateResponse(float64(3.142), errors.New("hello"), "")
	s.Equal(float64(3.142), response["result"])
	s.Equal("hello", response["error"])
	s.Len(response["message"], 0)

	response = CreateResponse(true, errors.New(""), "some message")
	s.Equal(true, response["result"])
	s.NotNil(response["error"])
	s.Len(response["error"], 0)
	s.Equal("some message", response["message"])

	response = CreateResponse("string value", nil, "")
	s.Equal("string value", response["result"])

	response = CreateResponse(nil, nil, "")
	s.Nil(response["result"])
}

func (s *UtilsTests) Test_HandleError_returnsFalseIfNoError() {
	w := httptest.NewRecorder()
	s.False(HandleError(nil, w))
}

func (s *UtilsTests) Test_HandleError_returnsTrueOnError() {
	w := httptest.NewRecorder()
	s.True(HandleError(errors.New("error"), w))
}

func (s *UtilsTests) Test_HandleError_addsCorrectHeaders() {
	w := httptest.NewRecorder()
	HandleError(errors.New("error"), w)
	response := w.Result()
	s.Equal(http.StatusInternalServerError, response.StatusCode)
	s.Equal("application/json", w.Header().Get("Content-Type"))
}

func (s *UtilsTests) Test_HandleResponse_addsCorrectHeaders() {
	w := httptest.NewRecorder()
	HandleResponse(1, w)
	result := w.Result()
	s.Equal(http.StatusOK, result.StatusCode)
	s.Equal("application/json", w.Header().Get("Content-Type"))
}

func (s *UtilsTests) Test_HandleResponse_sendsValidJSON() {
	var response map[string]interface{}
	w := httptest.NewRecorder()

	floatResult := float64(64.123)
	HandleResponse(floatResult, w)
	err := json.Unmarshal(w.Body.Bytes(), &response)
	s.Nil(err)

	w = httptest.NewRecorder()
	intResult := int(5)
	HandleResponse(intResult, w)
	err = json.Unmarshal(w.Body.Bytes(), &response)
	s.Nil(err)

	w = httptest.NewRecorder()
	boolResult := true
	HandleResponse(boolResult, w)
	err = json.Unmarshal(w.Body.Bytes(), &response)
	s.Nil(err)

	w = httptest.NewRecorder()
	stringResult := "string"
	HandleResponse(stringResult, w)
	err = json.Unmarshal(w.Body.Bytes(), &response)
	s.Nil(err)
}
