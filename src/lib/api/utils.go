package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

// CreateResponse returns a standardised response format to
// be passed to the consumer
func CreateResponse(result interface{}, err error, message string) map[string]interface{} {
	response := map[string]interface{}{
		"result":  result,
		"error":   nil, // keep as nil incase of consumer `null` typechecks
		"message": message,
	}
	if err != nil {
		response["error"] = err.Error()
	}
	return response
}

// HandleError responds to the consumer if :err is a valid value
// and returns true if an error was handled, false otherwise
func HandleError(err error, w http.ResponseWriter) bool {
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Add("Content-Type", "application/json")
		response := CreateResponse(nil, err, "")
		responseBody, err := json.Marshal(response)
		if err != nil {
			responseBody = []byte(
				fmt.Sprintf(
					`{"result":null,"error":"%s","message":"error parsing response as json"}`,
					url.QueryEscape(err.Error()),
				),
			)
		}
		w.Write(responseBody)
		return true
	}
	return false
}

// HandleResponse writes the appropriate response into the provided
// http.ResponseWriter
func HandleResponse(result interface{}, w http.ResponseWriter) {
	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	response := CreateResponse(result, nil, "")
	responseBody, err := json.Marshal(response)
	if HandleError(err, w) {
		return
	}
	w.Write(responseBody)
}
